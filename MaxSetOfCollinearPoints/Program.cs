﻿using MaxCollinearPoints;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;

namespace MaxSetOfCollinearPoints
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Node> points;
            using (var reader = new StreamReader("Data/punkty1.csv"))
            {
                points = new List<Node>();
                //points = new List<Node>() {
                //                            new Node() { CurrentPoint = new Point(2, 2)},
                //                            new Node() { CurrentPoint = new Point(3, 1)},
                //                            new Node() { CurrentPoint = new Point(5, -1)},
                //                            new Node() { CurrentPoint = new Point(7, -3)},
                //                            new Node() { CurrentPoint = new Point(1, -1)},
                //                            new Node() { CurrentPoint = new Point(1, -4)},
                //                            new Node() { CurrentPoint = new Point(1, -6)},             
                //};
                double x, y;
                int count = 0;
                while (!reader.EndOfStream)
                {
                    if (count == 1000) break;
                    var line = reader.ReadLine();
                    if (line != "")
                    {
                        var values = line.Split(';');
                        x = double.Parse(values[0], CultureInfo.InvariantCulture);
                        y = double.Parse(values[1], CultureInfo.InvariantCulture);

                        points.Add(new Node() { CurrentPoint = new Point(x, y) });
                        Console.WriteLine(values[0] + " " + values[1]);
                        count++;
                    }
                }
                Console.WriteLine(count);
            }
            MyTime.Start = DateTime.Now;
            CollinearilyCalculator collinearity = new CollinearilyCalculator(points);

            Console.WriteLine(" max number of collinear points: " + collinearity.SetAnglesForPoints());
            TimeSpan span = DateTime.Now - MyTime.Start;
            Console.WriteLine("time: " + span.TotalSeconds);
            Console.ReadKey();
        }
    }
    public static class MyTime
    {
        public static DateTime Start { get; set; }
    }
}
