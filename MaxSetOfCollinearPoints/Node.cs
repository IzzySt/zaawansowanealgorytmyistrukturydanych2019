﻿using MaxSetOfCollinearPoints;
using System.Collections.Generic;

namespace MaxCollinearPoints
{
    public class Node
    {
        public int Max { get; set; }

        public Point CurrentPoint;

        public List<AnglePoint> AnglesWithRestNodes;

        public Dictionary<double, int> AngleCountDict = new Dictionary<double, int>();
    }
}