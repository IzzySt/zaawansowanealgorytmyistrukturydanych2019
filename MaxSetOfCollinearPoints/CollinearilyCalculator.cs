﻿using MaxSetOfCollinearPoints;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MaxCollinearPoints
{
    public class CollinearilyCalculator
    {
        List<Node> nodes;

        public CollinearilyCalculator(List<Node> nodes)
        {
            this.nodes = nodes;
        }

        public double GetAngle(Point vec1, Point vec2)
        {
            if (vec1.Y < vec2.Y)
                return Math.Atan2(vec2.Y - vec1.Y, vec2.X - vec1.X);
            else
                return Math.Atan2(vec1.Y - vec2.Y, vec1.X - vec2.X);
        }

        public int SetAnglesForPoints()
        {
            for (int i = 0; i < nodes.Count - 1; ++i)
            {
                nodes[i].AnglesWithRestNodes = new List<AnglePoint>(nodes.Count);
                for (int j = i + 1; j < nodes.Count; ++j)
                {
                    var angle = GetAngle(nodes[i].CurrentPoint, nodes[j].CurrentPoint);
                    nodes[i].AnglesWithRestNodes.Add(new AnglePoint() { Point = nodes[j].CurrentPoint, Angle = angle });

                    CalculateNumberOfAngleOccurance(i, angle);
                }
                nodes[i].Max = nodes[i].AngleCountDict.Max(x => x.Value) +1 ;
            }
            return nodes.Max(x => x.Max);
        }

        private void CalculateNumberOfAngleOccurance(int i, double angle)
        {
            if (nodes[i].AngleCountDict.ContainsKey(angle))
                nodes[i].AngleCountDict[angle]++;
            else nodes[i].AngleCountDict.Add(angle, 1);
        }
    }
}