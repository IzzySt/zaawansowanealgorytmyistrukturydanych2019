﻿using MaxSetOfCollinearPoints;

namespace MaxCollinearPoints
{
    public class AnglePoint
    {
        public Point Point { get; set; }
        public double Angle { get; set; }
    }
}