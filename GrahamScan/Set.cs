﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace GrahamScan
{
    public class Set
    {
        private List<Point> points = new List<Point>();
        //1 - wyszukaj punkt który ma najmniejszy y (jeżeli jest takich kilka to najmniejszy x)
        public int minPoint;
        //2 - Punkt startowy ustawiamy na początku zbioru $ P $ ($ P[1] = S $). 
        //Sortujemy tablicę $ P[2..n] $ kątowo względem $ P[1] $. Oznacza to, że pierwszym elementem 
        //zbioru $ P[2..n] $ ma być punkt, który tworzy najmniejszy kąt z punktem $ P[1] $ 
        //i prostą równoległą do osi $ OX $ przechodzącą przez $ P[1] .
        public Set(List<Point> points)
        {
            this.points = points;
            minPoint = this.GetPositionOfMinPoint();
        }
        public int GetPositionOfMinPoint()
        {
            int iMin = Enumerable.Range(0, points.Count).Aggregate((jMin, jCur) =>
            {
                if (points[jCur].Y < points[jMin].Y)
                    return jCur;
                if (points[jCur].Y > points[jMin].Y)
                    return jMin;
                if (points[jCur].X < points[jMin].X)
                    return jCur;
                return jMin;
            });

            return iMin;
        }

        public bool IsLeft(Point s, Point a, Point b)
        {
            var result = ((s.X - a.X) * (b.Y - a.Y)) - ((s.Y - a.Y) * (b.X - a.X));
            return result < 0;
        }

        public List<Point> SortSet()
        {
            var sortQuery = Enumerable.Range(0, points.Count)
                .Where(i => i != minPoint)
                .Select(i => new KeyValuePair<double, Point>(Angle(points[minPoint], points[i]), points[i]))
                .OrderBy(pair => pair.Key)
                .Select(pair => pair.Value);

            List<Point> sortedList = new List<Point>(points.Count);
            sortedList.Add(points[minPoint]);
            sortedList.AddRange(sortQuery);

            return sortedList;
        }

        public double Angle(Point vec1, Point vec2)
        {
            return Math.Atan2(vec2.Y - vec1.Y, vec2.X - vec1.X) * 180 / Math.PI;
        }

        public List<Point> GrahamAlgorithm()
        {
            List<Point> p = SortSet();
            List<Point> omega = new List<Point>();

            omega.Add(p[0]);
            omega.Add(p[1]);

            var pt1 = p[0];
            int i = 2;
            while (i < p.Count)
            {
                var pt2 = omega.Last();
                if (this.IsLeft(pt1, pt2, p[i]))
                {
                    omega.Add(p[i]);
                    Console.WriteLine("dodalem do ścieżki punkt: " + p[i].X + " " + p[i].Y);
                    i++;
                }
                else
                {
                    omega.Remove(pt2);
                    Console.WriteLine("usunąłem punkt: " + pt2.X + " " + pt2.Y);
                }
                pt1 = omega[omega.Count - 2];
            }
            return omega;
        }
    }
}
