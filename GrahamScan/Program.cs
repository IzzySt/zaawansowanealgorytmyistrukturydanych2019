﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace GrahamScan
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Point> points = new List<Point>();
            Set set;
            using (var reader = new StreamReader("Data/punktyPrzykladowe.csv"))
            {
                points = new List<Point>();
                double x, y;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (line != "")
                    {
                        var values = line.Split(',');
                        x = double.Parse(values[0], CultureInfo.InvariantCulture);
                        y = double.Parse(values[1], CultureInfo.InvariantCulture);

                        points.Add(new Point(x, y));
                    }
                }
                set = new Set(points);
            }

            List<Point> grahamList = set.GrahamAlgorithm();
            Console.WriteLine("===================================================================");
            Console.WriteLine("Otoczka wypukła zbioru S:");

            grahamList.ForEach(k => Console.WriteLine(k.X + " " + k.Y));

            Console.ReadLine();
        }
    }
}
