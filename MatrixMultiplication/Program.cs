﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;

namespace MatrixMultiplication
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Stopwatch watch;
            long elapsedMs;
            var matrices = ReadFile("Data/matrixes.txt");
            var m0 = new Matrix(matrices[0].x, matrices[0].y);
            m0 = matrices[0];
            watch = System.Diagnostics.Stopwatch.StartNew();
            var synRes = MatrixHelper.SMultiplication(matrices, m0);
            watch.Stop();

            elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("wynik dla obliczen sekwencyjnych: ");
            Console.WriteLine("czas [ms]: " + elapsedMs);
            WriteMatrixOnConsole(synRes.A);

            Console.WriteLine("==========================================");

            var matrices2 = ReadFile("Data/matrixes.txt");
            watch = System.Diagnostics.Stopwatch.StartNew();
            var xx = MatrixHelper.TasksMultiplicationAsync(matrices2, 4).GetAwaiter().GetResult();
            var res = MatrixHelper.SMultiplication(xx, xx[0]);
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("wynik dla obliczen rownoleglych(Task): ");
            Console.WriteLine("czas [ms]: " + elapsedMs);
            WriteMatrixOnConsole(res.A);

            Console.WriteLine("==========================================");

            watch = System.Diagnostics.Stopwatch.StartNew();
            var x = MatrixHelper.ThreadsMultiplication(matrices2, 2);
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("wynik dla obliczen rownoleglych(Thread): ");
            Console.WriteLine("czas [ms]: " + elapsedMs);
            WriteMatrixOnConsole(x.A);
            Console.Read();
        }


        private static void WriteMatrixOnConsole(double[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + "  ");
                }
                Console.WriteLine();
            }
        }

        private static List<Matrix> ReadFile(string path)
        {
            using (var reader = new StreamReader(path))
            {
                int RowLenght, ColumnsLenght;
                var matrices = new List<Matrix>();

                string[] matricesStrings;
                using (var file = new StreamReader(path))
                {
                    var splittedMatrices = file.ReadToEnd();
                    matricesStrings = splittedMatrices.Split("-------------------------");
                    file.Close();
                }

                Matrix matrix = null;
                foreach (var mat in matricesStrings)
                {
                    var output = mat.Replace("\r\n", "");
                    var values = output.Split('[', ']');
                    var valuesWithoutEmpty = values.Where(x => !string.IsNullOrEmpty(x) && x != " ").ToArray();
                    var dimension = valuesWithoutEmpty[1].Split('x');
                    RowLenght = int.Parse(dimension[0], CultureInfo.InvariantCulture);
                    ColumnsLenght = int.Parse(dimension[1], CultureInfo.InvariantCulture);
                    matrix = new Matrix(RowLenght, ColumnsLenght);

                    int j = 0;
                    for (int i = 2; i < valuesWithoutEmpty.Length; i++)
                    {
                        if (valuesWithoutEmpty[i] != "" && valuesWithoutEmpty[i] != " ")
                        {
                            var rowSplit = valuesWithoutEmpty[i].Split(' ');
                            var row = rowSplit.Where(x => !string.IsNullOrEmpty(x) && x != " ").ToArray();
                            for (int column = 0; column < ColumnsLenght; column++)
                            {
                                matrix.A[j, column] = double.Parse(row[column], CultureInfo.InvariantCulture);
                            }
                            ++j;
                        }
                    }
                    matrices.Add(matrix);
                }
                return matrices;
            }   
        }
    }
}
