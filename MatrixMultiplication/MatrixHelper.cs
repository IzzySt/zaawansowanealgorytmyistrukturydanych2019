﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MatrixMultiplication
{
    public static class MatrixHelper
    {
        private static object syncObj = new object();

        public static Matrix SMultiplication(List<Matrix> matrices, Matrix firstMatrix)
        {
            var result = firstMatrix;
            for (int i = 1; i < matrices.Count; i++)
            {
                result.A = MultiplyMatrix(result.A, matrices[i].A);
            }
            return result;
        }

        public static Task<Matrix> SMultiplicationTask(List<Matrix> matrices, Matrix firstMatrix)
        {
            var result = matrices[0];

            double[,] a = matrices[0].A;
            double[,] b;
            double[,] c = null;
            for (int m = 1; m < matrices.Count; m++)
            {
                b = matrices[m].A;

                if (a.GetLength(1) == b.GetLength(0))
                {
                    c = new double[a.GetLength(0), b.GetLength(1)];
                    for (int i = 0; i < c.GetLength(0); i++)
                    {
                        for (int j = 0; j < c.GetLength(1); j++)
                        {
                            c[i, j] = 0;
                            for (int k = 0; k < a.GetLength(1); k++) // OR k<b.GetLength(0)
                                c[i, j] = c[i, j] + a[i, k] * b[k, j];
                        }
                    }
                    a = c;
                }
                else
                {
                    Console.WriteLine("\n Number of columns in First Matrix should be equal to Number of rows in Second Matrix.");
                    Console.WriteLine("\n Please re-enter correct dimensions.");
                    Console.ReadKey();
                    Environment.Exit(-1);
                    return null;
                }
            }
            result = new Matrix(c.GetLength(0), c.GetLength(1));
            result.A = c;
            return Task.FromResult(result);
        }

        public static EventWaitHandle setResult = new AutoResetEvent(false);
        public static EventWaitHandle readyForResult = new AutoResetEvent(false);


        public async static Task<List<Matrix>> TasksMultiplicationAsync(List<Matrix> matrices, int threads)
        {
            var matrixRange = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(matrices.Count / threads)));
            var max = matrices.Count;
            var runningTasks = new List<Task<Matrix>>();
            var resultt = new List<Matrix>();

            for (var i = 0; i < max; i += matrixRange)
            {
                int i2 = i;
                ;
                runningTasks.Add(Task<Matrix>.Factory.StartNew((parameter) =>
                {
                    int ii = (int)parameter;

                    var partOfMatrices = matrices.GetRange(ii, matrixRange);
                    var r = partOfMatrices[0];
                    var x = SMultiplicationTask(partOfMatrices, r).Result;
                    return x;
                }, i2
                ));

            }
            var result = await Task.WhenAll(runningTasks);
            return result.ToList();

        }

        public static List<Matrix> TasksMultiplicationAsyncTest(List<Matrix> matrices, int threads)
        {
            var matrixRange = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(matrices.Count / threads)));
            var max = matrices.Count;
            var runningTasks = new List<Task<Matrix>>();
            var resultt = new List<Matrix>();

            var partOfMatrices = matrices.GetRange(0, 5000);
            var x = SMultiplicationTask(partOfMatrices, partOfMatrices[0]).Result;
            resultt.Add(x);
            var partOfMatrices2 = matrices.GetRange(5000, 5000);
            x = SMultiplicationTask(partOfMatrices2, partOfMatrices2[0]).Result;
            resultt.Add(x);
            //var result = await Task.WhenAll(runningTasks);
            return resultt.ToList();

        }

        public static Matrix ThreadsMultiplication(List<Matrix> matrices, int threads)
        {
            var matrixRange = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(matrices.Count / threads)));
            var max = matrices.Count;
            var resultt = new List<Matrix>();
            Matrix result = null;
            var runningTasks = new List<Task>();

            Parallel.For(0, threads, p =>
            {
                //readyForResult.WaitOne();
                lock (syncObj)
                {
                    var partOfMatrices = matrices.GetRange(p, matrixRange);
                    var r = partOfMatrices[0];
                    var x = SMultiplicationTask(partOfMatrices, r).Result;
                    result = matrices[0];

                    double[,] a = matrices[0].A;
                    double[,] b;
                    double[,] c = null;
                    for (int m = 1; m < matrices.Count; m++)
                    {
                        b = matrices[m].A;

                        if (a.GetLength(1) == b.GetLength(0))
                        {
                            c = new double[a.GetLength(0), b.GetLength(1)];
                            for (int i = 0; i < c.GetLength(0); i++)
                            {
                                for (int j = 0; j < c.GetLength(1); j++)
                                {
                                    c[i, j] = 0;
                                    for (int k = 0; k < a.GetLength(1); k++) // OR k<b.GetLength(0)
                                        c[i, j] = c[i, j] + a[i, k] * b[k, j];
                                }
                            }
                            a = c;
                        }
                        else
                        {
                            Console.WriteLine("\n Number of columns in First Matrix should be equal to Number of rows in Second Matrix.");
                            Console.WriteLine("\n Please re-enter correct dimensions.");
                            Console.ReadKey();
                            Environment.Exit(-1);
                        }
                    }
                    result = new Matrix(c.GetLength(0), c.GetLength(1));
                    result.A = c;
                }
                // setResult.Set();
            });
            return result;
            return SMultiplication(resultt, resultt[0]);
        }

        public static double[,] MultiplyMatrix(double[,] a, double[,] b)
        {
            if (a.GetLength(1) == b.GetLength(0))
            {
                double[,] c = new double[a.GetLength(0), b.GetLength(1)];
                for (int i = 0; i < c.GetLength(0); i++)
                {
                    for (int j = 0; j < c.GetLength(1); j++)
                    {
                        c[i, j] = 0;
                        for (int k = 0; k < a.GetLength(1); k++) // OR k<b.GetLength(0)
                            c[i, j] = c[i, j] + a[i, k] * b[k, j];
                    }
                }
                return c;
            }
            else
            {
                Console.WriteLine("\n Number of columns in First Matrix should be equal to Number of rows in Second Matrix.");
                Console.WriteLine("\n Please re-enter correct dimensions.");
                Environment.Exit(-1);
                return null;
            }
        }

    }
}
