﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixMultiplication
{
    public class Matrix
    {
        public int x, y;

        public double[,] A;

        public Matrix(int x, int y)
        {
            this.x = x;
            this.y = y;
            this.A = new double[x, y];
        }


    }
}
