﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace BoyeraMoore
{
    class Program
    {
        static void Main(string[] args)
        {
            string senaca = ReadFile("Data/senaca.txt");
            var trimmedText = SplitInputIntoWords(senaca);
            var count = 0;
            foreach (var word in trimmedText)
            {
                if(word.Contains("cice"))
                {
                    ++count;
                    Console.WriteLine(count + " " + word);
                }
            }
            if (count == 0)
                Console.WriteLine("Brak pasujących wyrażeń!");
            Console.Read();
        }

        static List<string> SplitInputIntoWords(string input)
        {
            input = input.ToLower();
            var punctuation = input.Where(char.IsPunctuation).Distinct().ToArray();
            var words = input.Split().Select(x => x.Trim(punctuation)).ToList();
            return words;
        }

        static string ReadFile(string path)
        {
            string dataFromFile = null;
            using (var file = new StreamReader(path))
            {
                dataFromFile = file.ReadToEnd();
                file.Close();
            }
            return dataFromFile;
        }
    }
}
