﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Huffmann
{
    //drugi przypadek - segregowanie po dwa znaki (teraz jest po jednym)
    class Program
    {

        static void Main(string[] args)
        {
            //var huffmann = new HuffmannAlgo(ReadFile("data/inputFile.txt"));
            //var huffmann = new HuffmannAlgo("TO BE OR NOT TO BE", true);
            var huffmann = new HuffmannAlgo(ReadFile("data/vuldat.txt"));
            Console.ReadKey();
        }

        static string ReadFile(string path)
        {
            string dataFromFile = null;
            using (var file = new StreamReader(path))
            {
                dataFromFile = file.ReadToEnd();
                file.Close();
            }
            return dataFromFile;
        }

    }
}
