﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Huffmann
{
    public static class HuffmannTreeHelper
    {
        public static Dictionary<string, StringBuilder> treeHash;
        public static string ReadFile(string path)
        {
            var dataFromFile = new StringBuilder();
            using (var file = new StreamReader(path))
            {
                int counter = 0;
                string ln;

                while ((ln = file.ReadLine()) != null)
                {
                    dataFromFile.Append(ln);
                    counter++;
                }
                file.Close();
            }

            return dataFromFile.ToString();
        }
    }
}
