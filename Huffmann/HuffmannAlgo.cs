﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Huffmann
{
    public class HuffmannAlgo
    {
        private string inputText;
        private Node root = new Node();
        private List<Node> nodesInput = new List<Node>();
        public static Dictionary<string, byte[]> treeHash;
        private long memorySizeOfTree;
        //(inputtext - (bintext + treelen) )/ input text len
        public HuffmannAlgo(string inputText)
        {
            this.inputText = inputText;
            treeHash = new Dictionary<string, byte[]>();
            GetBestCompressionLevel();
        }

        private void GetBestCompressionLevel()
        {
            var i = 1;
            double nextCompressionLevel = 1;
            this.nodesInput = this.GetSortedNodesWithFrequencyOfLetters(i);
            this.CreateDigitTree(this.nodesInput);
            treeHash = new Dictionary<string, byte[]>();
            this.AssignCodesToTree();
            
            this.Encode(inputText, i);
            var binaryOutput = HuffmannTreeHelper.ReadFile("data/encodedText.txt");
            this.Decode(binaryOutput);
            var compressionLevel1 = this.CountCompressionLevel(inputText.Length, binaryOutput.Length, memorySizeOfTree);
            Console.WriteLine("liczba kodowanych symboli: " + i);
            Console.WriteLine("Poziom kompresji: " + compressionLevel1);

            while (nextCompressionLevel > compressionLevel1)
            {
                 nextCompressionLevel = compressionLevel1;
                ++i;
                this.nodesInput = this.GetSortedNodesWithFrequencyOfLetters(i);
                this.CreateDigitTree(this.nodesInput);
                treeHash = new Dictionary<string, byte[]>();
                this.AssignCodesToTree();
                this.Encode(inputText, i);
                binaryOutput = HuffmannTreeHelper.ReadFile("data/encodedText.txt");
                this.Decode(binaryOutput);
                compressionLevel1 = this.CountCompressionLevel(inputText.Length, binaryOutput.Length, memorySizeOfTree);

                Console.WriteLine("liczba kodowanych symboli: " + i);
                Console.WriteLine("Poziom kompresji: " + compressionLevel1);
                if (i == 2)
                    break;
            }

            //Console.WriteLine("Najlepsza długość:" + i);

        }

        private string Decode(string bits) // dekoduj
        {
            Node current = this.root;
            string decoded = "";
            using (var writeText = new StreamWriter("data/decodedText.txt"))
            {
                foreach (var bit in bits)
                {
                    if (bit == 1)
                    {
                        if (current.RightNode != null)
                        {
                            current = current.RightNode;
                        }
                    }
                    else
                    {
                        if (current.LeftNode != null)
                        {
                            current = current.LeftNode;
                        }
                    }

                    if (current.LeftNode == null && current.RightNode == null)
                    {
                        writeText.Write(current.Symbol);
                        current = this.root;
                    }
                }
            }

            return decoded;
        }

        private void Encode(string inputText, int codeLenght) // zakoduj 
        {
            var inputTextLenght = inputText.Length;
            using (var writeText = new StreamWriter("data/encodedText.txt"))
            {
                for (int i = 0; i <= inputTextLenght - codeLenght; i += codeLenght)
                {
                    string symbol = inputText.Substring(i, codeLenght);
                    var code = treeHash[symbol];
                    writeText.Write(Encoding.UTF8.GetString(code, 0, code.Length));
                }
            }
        }

        private void AssignCodesToTree()
        {
            byte[] encoded = { };
            var tree = this.CreateDigitTree(this.nodesInput);
            var memoryStateBefore = GC.GetTotalMemory(true);
            this.root.CodingSymbol(this.root, encoded, treeHash);
            var memoryStateAfter = GC.GetTotalMemory(true);
            //this.memorySizeOfTree = memoryStateAfter - memoryStateBefore; // checked memory by GC
            this.memorySizeOfTree = this.GetSizeOfTree();           
        }
        private Node CreateDigitTree(List<Node> tree)
        {
            if (tree.Count < 2)
            {
                return this.root;
            }

            if (tree.Count == 2)
            {
                var sumNodeFreq = new Node() { Frequency = tree[0].Frequency + tree[1].Frequency, RightNode = tree[0], LeftNode = tree[1] };
                sumNodeFreq.RightNode.Code = 0;
                sumNodeFreq.LeftNode.Code = 1;
                this.root = sumNodeFreq;
                return this.root;
            }
            else
            {
                tree.Sort();
                var node1 = tree[0];
                var node2 = tree[1];
                var sumNodeFreq = new Node() { Frequency = node1.Frequency + node2.Frequency };
                sumNodeFreq.RightNode = node1;
                sumNodeFreq.LeftNode = node2;
                sumNodeFreq.RightNode.Code = 0;
                sumNodeFreq.LeftNode.Code = 1;

                tree.RemoveAt(0);
                tree.RemoveAt(0);
                tree.Add(sumNodeFreq);
                tree.Sort();
                this.CreateDigitTree(tree);
                return this.root;
            }
        }

        private List<Node> GetSortedNodesWithFrequencyOfLetters(int numberOfLetters)
        {
            var nodes = new List<Node>();
            var digitFrequency = new Dictionary<string, int>();
            for (int i = 0; i <= this.inputText.Length - numberOfLetters; i+=numberOfLetters)
            {
                var subString = this.inputText.Substring(i, numberOfLetters);
                if (digitFrequency.ContainsKey(subString))
                {
                    digitFrequency[subString]++;
                }
                else
                {
                    digitFrequency.Add(subString, 1);
                }
            }
            //foreach (var letter in this.inputText.ToCharArray())
            //{
            //    if (digitFrequency.ContainsKey(letter.ToString()))
            //    {
            //        digitFrequency[letter.ToString()]++;
            //    }
            //    else
            //    {
            //        digitFrequency.Add(letter.ToString(), 1);
            //    }
            //}

            foreach (KeyValuePair<string, int> digitFreq in digitFrequency)
            {
                nodes.Add(new Node() { Symbol = digitFreq.Key, Frequency = digitFreq.Value });
            }
            return nodes;
        }

        private double CountCompressionLevel(int inputTextLenght, int binaryTextLenght, long sizeOfTreeInMemory)
        {
            var inputTextAsciiLenght = inputTextLenght * 8;
            return (Convert.ToDouble(inputTextAsciiLenght) - (Convert.ToDouble(binaryTextLenght) + Convert.ToDouble(sizeOfTreeInMemory))) / Convert.ToDouble(inputTextAsciiLenght);
        }

        // Calculates the lenght in bytes of an object 
        private long GetSizeOfTree()
        {
            long size = 0;
            using (Stream s = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(s, treeHash);
                size = s.Length;
            }
            return size;
        }
    }
}
