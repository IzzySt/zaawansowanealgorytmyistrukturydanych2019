﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Huffmann
{
    public class Node: IComparable<Node>
    {
        public long Frequency { get; set; }
        public Node LeftNode { get; set; }
        public Node RightNode { get; set; }
        public string Symbol { get; set; }
        public int Code { get; set; }


        public int CompareTo(Node other)
        {
            if (this.Frequency > other.Frequency)
            {
                return 1;
            }

            if (this.Frequency < other.Frequency)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        public void CodingSymbol(Node node, byte[] encoded, Dictionary<string, byte[]> tree)
        {
            if (node.LeftNode == null && node.RightNode == null)
            {
                if (node.Symbol != null)
                {
                    tree.Add(this.Symbol, encoded);
                }
            }

            if (this.LeftNode != null)
            {
                Array.Resize(ref encoded, encoded.Length + 1);              
                encoded[encoded.Length - 1] = 0;
                this.LeftNode.CodingSymbol(node.LeftNode, encoded, tree);

                var tmp = new List<byte>(encoded);
                tmp.RemoveAt(encoded.Length - 1);
                encoded = tmp.ToArray();
            }

            if (this.RightNode != null)
            {
                Array.Resize(ref encoded, encoded.Length + 1);
                encoded[encoded.Length-1] = 1;
                this.RightNode.CodingSymbol(node.RightNode, encoded, tree);

                var tmp = new List<byte>(encoded);
                tmp.RemoveAt(encoded.Length - 1);
                encoded = tmp.ToArray();
            }
        }
    }
}
